Resposta 4:

Um deadlock acontece quando dois ou mais processos são bloqueados por tentarem acessar um recurso que esta em uso por outro processo e isso faz com que os outros processos fiquem aguardando a liberação do recurso.

Arquitetar uma aplicação evitando o compartilhamento de recursos por "n" processos pode contribuir para que você não tenha deadlocks, mas caso isso não seja possivel, evitar o uso de processamento em paralelo, fazer uso de filas para processamento, processamento assincrono, uso da API de Stream, paradigma de imutabilidade, ordenação de processos e timeout para esses processamentos são abordagens que podem ajudar.

Alguns recursos para evitar deadlocks estão disponiveis em frameworks ORM e a nivel de banco de dados também podem ser uma opção.