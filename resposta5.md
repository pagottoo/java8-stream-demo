Resposta 5:
Processamento de Streams em paralelo com o Java 8 se dá ao chamarmos o método foo.parallelStream().bar() por exemplo ao invés de foo.stream.bar().

Isso faz com que a JVM processe paralelamente nossa Stream de acordo com o numero de cores de CPU disponiveis.

Porém sabemos que multi threads e processamento paralelo são custosos, então vamos entender em quais cenarios faz sentido usarmos o parallelStream().

O uso de parallelStream pode cair bem quando temos muitos nucleos de CPU, ou quando a massa de dados a ser processada é grande o suficiente para valer a pena se processar em paralelo.

O processo de "splitar" uma Stream, fazer o processamento em "n" threads, criar objetos, depois juntar as tasks e limpar estes objetos (GC) causa um grande overhead. Nesse ponto o processamento serial pode ser mais rapido que o paralelo.

Por outro lado também devemos levar em consideração que mesmo uma  pequena massa de dados, mas que o processamento de cada elemento seja "pesado" o suficiente, pode compensar o parallelStream.

Outro ponto que devemos nos atentar no parallelStream, é como implementamos nossas funções, isso porque o processamento paralelo não pode alterar os elementos da lista, sendo independente não intereferindo nos elementos.

Por fim, podemos separar o uso de parallelStream() pelo tipo de Stream que vamos processar.

Em quais tipos de lista faz sentido usar o parallelStream():
ArrayList, HashMap e Arrays de uma dimensão.

Quando não faz sentido usar parallelStream():
LinkedList, BlockingQueues e fontes de Stream que fazem muito I/O.

Todos os outros tipos de lista podem entrar no bom uso do parallelStream().